<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function show(){
        return "Hellow Aniruddh";
    }

    public function shows($name){
        return "Hellow : ".$name;
    }

    public function about(){
        return view('about');
    }

    public function profile(){
        return view('admin.profile');
    }

    public function contact($number){
        return view('contact',['number'=>$number]);
    }

    public function programming(){
        $programming="Laravel";
        return view('programming',['pro'=>$programming]);
    }
}
