<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DataAni;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('home',[HomeController::class, 'show']);
Route::get('homes/{name}',[HomeController::class, 'shows']);
Route::get('about',[HomeController::class, 'about']);
Route::get('profile',[HomeController::class, 'profile']);
Route::get('contact/{number}',[HomeController::class, 'contact']);
Route::get('programming',[HomeController::class, 'programming']);

// --invokable Single Action Controller
Route::get('datas',DataAni::class);
